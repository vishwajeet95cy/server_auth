const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const app = express();
const dotenv = require('dotenv');
dotenv.config()
const authRoutes = require('./routes/authRoutes')
const fs = require('fs');
const options = {
  key: fs.readFileSync('./ssl/heycalls.pem'),
  cert: fs.readFileSync('./ssl/heycalls-cert.pem')
};
const server = require('https').createServer(options, app)
const cookie = require('cookie-parser');
const session = require('express-session');


app.use(morgan('dev'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

app.use(cookie());
app.use(session({
  secret: process.env.Session_secret,
  resave: true,
  saveUninitialized: true,
  cookie: { maxAge: 24 * 60 * 60, secure: true }
}))

app.get('/', (req, res, next) => {
  res.send("Welcome to Express Server")
})


app.use('/auth/user', authRoutes);

server.listen('8000', (req, res) => {
  console.log('Server is listening at http://localhost:8000');
})