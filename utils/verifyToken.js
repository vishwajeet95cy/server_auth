const jwt = require('jsonwebtoken');


module.exports = function (req, res, next) {
  const token = req.header('auth-token');
  console.log(token);

  // if (!token) return res.status(401).send('Access Denied');

  try {
    const verified = jwt.verify(token, process.env.Token_secret);
    console.log(verified)
    req.user = verified;
    //res.redirect('https://panel.heycalls.com');
  } catch (err) {

    // res.status(400).send('Invalid Token');
    //   res.redirect('https://heycalls.com')
  }
  next();
}

