const router = require('express').Router();
const verifyToken = require('../utils/verifyToken');
const jwt = require('jsonwebtoken');
const LocalStorage = require('node-localstorage').LocalStorage


router.get('/:token', (req, res) => {
  // console.log("request", req);
  console.log("User", req.params.token)
  const decode = {};

  try {
    decode = jwt.verify(req.params.token, process.env.Token_secret);
    console.log(decode);
  } catch {
    //  res.redirect('https://heycalls.com');
  }
  if (decode) {
    // var localStorage = new LocalStorage('./scratch')
    // localStorage.setItem('auth', req.params.token)
    // console.log(localStorage.getItem('auth'))
    res.cookie('auth', req.params.token, { expires: new Date(Date.now() + 900000), secure: true, httpOnly: true })
    res.redirect('https://panel.heycalls.com/');
  } else {
    res.redirect('https://heycalls.com');
  }
})



module.exports = router